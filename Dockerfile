FROM openjdk:8

MAINTAINER TOM-12 <tomasz12@gmail.com>

ARG SERVICE_DISCOVERY_HOSTNAME
ARG SERVICE_DISCOVERY_PORT

VOLUME /tmp
ADD target/service-discovery*.jar service-discovery.jar

ENV SERVICE_DISCOVERY_HOSTNAME=${SERVICE_DISCOVERY_HOSTNAME}
ENV SERVICE_DISCOVERY_PORT=${SERVICE_DISCOVERY_PORT}

RUN sh -c 'touch /service-discovery.jar'
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /service-discovery.jar" ]
